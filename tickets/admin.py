from django.contrib import admin
from tickets.models import Flight


@admin.register(Flight)
class FlightAdmin(admin.ModelAdmin):
    list_display = ('name',)
