from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'conf.settings')

app = Celery('proj')

app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'update-flights-every-midnight': {
        'task': 'update_flights',
        'schedule': crontab(minute=0, hour=0),
    }
}
app.conf.timezone = 'Asia/Almaty'
