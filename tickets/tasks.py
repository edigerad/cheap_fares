import logging
import datetime

from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from celery.decorators import task

from tickets.models import Flight
from tickets.connections import SkypickerConnection

logger = logging.getLogger('info_logger')


@task(name="update_flights")
def update_flights():
    """
        update all top 10 flights prices for upcoming 30  days  by using data from skypicker api
    """
    cache.clear()
    flights = Flight.objects.all()
    [update_flight.delay(flight.name) for flight in flights]
    return 'Update of all flights is finished'


@task(name="update_flight")
def update_flight(flight_name):
    """
        update the given flight's min prices for upcoming 30 days by using data from skypicker api
    """
    prices = []
    date = datetime.datetime.now()
    conn = SkypickerConnection()
    for n in range(30):
        date_formatted = date.strftime("%d/%m/%Y")
        tickets = conn.get_tickets(flight_name, date_formatted)
        for ticket in tickets:  # from cheapest to expensive prices
            # get confirmed data about the ticket
            confirmed_ticket = conn.confirm_ticket(token=ticket['booking_token'])

            # check if confirmed_ticket is valid or not
            if confirmed_ticket['flights_invalid']:
                continue

            # get actual price if price is changed otherwise get min price
            min_price = confirmed_ticket['flights_price'] if confirmed_ticket['price_change'] else ticket['data'][
                'price']

            prices += [{
                'price': min_price,
                'date': date,
            }]

            # break if everything is OK with cheapest price
            break
        date += datetime.timedelta(days=1)

    cache.set(flight_name, prices, timeout=DEFAULT_TIMEOUT)
    return 'Update of %s is finished' % (flight_name,)
