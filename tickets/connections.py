import logging
import requests
import json

logger = logging.getLogger("info_logger")


class SkypickerConnection(object):
    """
        Used to retrieve data from skypicker
    """
    url = 'https://api.skypicker.com/flights'
    confirm_url = 'https://booking-api.skypicker.com/api/v0.1/check_flights'

    def get_tickets(self, flight, date):
        print(flight.split('-'))
        params = {
            'fly_from': flight.split('-')[0],
            'fly_to': flight.split('-')[1],
            'date_from': date,
            'date_to': date,
            'partner': 'picky',
            'adults': 1
        }
        response = requests.get(self.url, params=params)
        return response.json()['data']

    def confirm_ticket(self, token):
        params = {
            'booking_token': token,
            'bnum': 1,
            'adults': 1
        }
        response = {'flights_checked': False}
        while response['flights_checked'] is False:
            response = requests.get(self.confirm_url, params=params).json()
        return response
