from django.db import models


class Flight(models.Model):
    name = models.CharField(max_length=7)

    def __str__(self):
        return self.name