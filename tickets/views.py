from django.http import HttpResponse

from tickets.models import Flight

from django.core.cache import cache


def tickets_view(request):
    tickets = ''
    for flight in Flight.objects.all():
        tickets += '%s: %s<br>' % (flight.name, cache.get(flight.name))
    return HttpResponse(tickets)
