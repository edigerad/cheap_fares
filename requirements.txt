Django==3.1.4
django-celery==3.3.1
django-celery-beat==2.1.0
django-celery-results==2.0.0
pytz==2020.4
requests==2.25.0


