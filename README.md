# CF

The cheap fares for 30 days among top 10 flights, including Almaty, Astana, Shymkent, Petersburg and Moscow.

- python 3.8, pip, virtualenv

##  Instructions for use and launch app.

1. Configure virtual environment settings

    1.1 Create virtual environment and activate it
    
    ```bash
    virtualenv -p python3.8 .venv
   source .venv/bin/activate
    ```
   
    1.2 Install required libraries

    ```bash
    pip install ./requirements.txt
    ```
2. Create super user to view top 10 flights from admin panel and migrate

    ```bash
    ./manage.py createsuperuser
    ```

    ```bash
    ./manage.py migrate
    ```
3. Run the server

    ```bash
    ./manage.py runserver
    ```

4. Run worker manually to be sure that celery is correct and runnable

    ```bash
    celery -A tickets worker -l info
    ```
    
    